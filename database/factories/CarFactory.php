<?php

namespace Database\Factories;

use App\Models\CarModel;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $model = CarModel::factory()->create();
        return [
            'mileage' => fake()->randomNumber(),
            'production_year' => fake()->date,
            'color' => fake()->colorName,
            'model_id' => $model->id,
            'brand_id' => $model->brand->id,
            'user_id'  => User::factory()->create()->id
        ];
    }
}
