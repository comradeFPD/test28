<?php

namespace Database\Seeders;

use App\Models\Car;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Car::factory()->count(5)->create([
            'user_id' => 1
        ]);
        Car::factory()->count(5)->create([
            'user_id' => 2
        ]);
        Car::factory()->count(5)->create();
    }
}
