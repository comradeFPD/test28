<?php

use App\Http\Controllers\Api\v1\BrandController;
use App\Http\Controllers\Api\v1\CarController;
use App\Http\Controllers\Api\v1\LoginController;
use App\Http\Controllers\Api\v1\ModelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('car-brand', BrandController::class);
Route::resource('car-model', ModelController::class);
Route::resource('car', CarController::class)
    ->except([
        'create',
        'update',
        'destroy'
    ]);
Route::resource('car', CarController::class)->only([
    'create',
    'update',
    'destroy'
])->middleware('auth:sanctum');
Route::get('my-cars', [CarController::class, 'viewMyCars'])
    ->name('car.my-cars')->middleware('auth:sanctum');
Route::post('login', LoginController::class)
    ->name('login');
