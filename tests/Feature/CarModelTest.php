<?php

namespace Tests\Feature;

use App\Models\CarModel;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CarModelTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /**
     * @test Can I create new car model
     *
     * @return void
     */
    public function canICreateNewCarModel() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $data = CarModel::factory()->make()->toArray();
        $response = $this->postJson(route('car-model.store'), $data);
        $response->assertCreated();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseHas('car_models', [
            'name' => $data['name'],
            'brand_id' => $data['brand_id']
        ]);
    }

    /**
     * @test Can I update car model
     *
     * @return void
     */
    public function canIUpdateCarModel() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $carModel = CarModel::factory()->create();
        $data = $carModel->toArray();
        $data['name'] = 'new model name';
        $response = $this->putJson(route('car-model.update', $carModel->id), $data);
        $response->assertOk();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseHas('car_models', [
            'id' => $carModel->id,
            'name' => $data['name']
        ]);
    }

    /**
     * @test Can I delete car model
     *
     * @return void
     */
    public function canIDeleteCarModel() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $carModel = CarModel::factory()->create();
        $response = $this->deleteJson(route('car-model.destroy', $carModel->id));
        $response->assertSuccessful();
        $this->assertDatabaseMissing('car_models', [
            'id' => $carModel->id
        ]);
    }

    /**
     * @test Can I see all car models
     *
     * @return void
     */
    public function canISeeAllCarModels() : void
    {
        CarModel::factory()->count(10)->create();
        $response = $this->getJson(route('car-model.index'));
        $response->assertOk();
    }

    /**
     * @test Can I see single car model
     *
     * @return void
     */
    public function canISeeSingleCarModel() : void
    {
        $model = CarModel::factory()->create();
        $response = $this->getJson(route('car-model.show', $model->id));
        $response->assertOk();
    }

    /**
     * @test Can I create car model with not existing brand
     *
     * @return void
     */
    public function canICreateCarModelWithNotExistingBrand() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $data = CarModel::factory()->make([
            'brand_id' => 123
        ])->toArray();
        $response = $this->postJson(route('car-model.store'), $data);
        $response->assertUnprocessable();
        $response->assertJsonValidationErrors('brand_id');
        $this->assertDatabaseMissing('car_models', [
            'name' => $data['name'],
            'brand_id' => $data['brand_id']
        ]);
    }
}
