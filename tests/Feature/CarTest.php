<?php

namespace Tests\Feature;

use App\Models\Car;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CarTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /**
     * @test Can I create car
     *
     * @return void
     */
    public function canICreateCar() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $data = Car::factory()->make()->toArray();
        $response = $this->postJson(route('car.store'), $data);
        $response->assertCreated();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseHas('cars', [
            'brand_id' => $data['brand_id'],
            'model_id' => $data['model_id'],
        ]);
    }

    /**
     * @test Can I update car
     *
     * @return void
     */
    public function canIUpdateCar() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $car = Car::factory()->create([
            'user_id' => $user->id
        ]);
        $data = $car->toArray();
        $data['color'] = fake()->colorName;
        $response = $this->putJson(route('car.update', $car->id), $data);
        $response->assertOk();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseHas('cars', [
            'id' => $car->id,
            'color' => $data['color']
        ]);
    }

    /**
     * @test Can I delete car
     *
     * @return void
     */
    public function canIDeleteCar() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $car = Car::factory()->create([
            'user_id' => $user->id
        ]);
        $response = $this->deleteJson(route('car.destroy', $car->id));
        $response->assertSuccessful();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseMissing('cars', [
            'id' => $car->id
        ]);
    }

    /**
     * @test Can I see all cars
     *
     * @return void
     */
    public function canISeeAllCars() : void
    {
        Car::factory()->count(10)->create();
        $response = $this->getJson(route('car.index'));
        $response->assertOk();
    }

    /**
     * @test Can I see single car
     *
     * @return void
     */
    public function canISeeSingleCar() : void
    {
        $car = Car::factory()->create();
        $response = $this->getJson(route('car.show', $car->id));
        $response->assertOk();
    }

    /**
     * @test Can I see my cars
     *
     * @return void
     */
    public function canISeeMyCars() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        Car::factory()->count(5)->create([
            'user_id' => $user->id
        ]);
        Car::factory()->count(5)->create();
        $response = $this->getJson(route('car.my-cars'));
        $response->assertOk();
    }

    /**
     * @test Can I delete not my car
     *
     * @return void
     */
    public function canIDeleteNotMyCar() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $car = Car::factory()->create();
        $response = $this->deleteJson(route('car.destroy', $car->id));
        $response->assertNotFound();
        $this->assertDatabaseHas('cars', [
            'id' => $car->id
        ]);
    }

    /**
     * @test Can I update not my car
     *
     * @return void
     */
    public function canIUpdateNotMyCar() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $car = Car::factory()->create();
        $data = $car->toArray();
        $data['color'] = fake()->colorName;
        $response = $this->putJson(route('car.update', $car->id), $data);
        $response->assertNotFound();
        $this->assertDatabaseMissing('cars', [
            'id'    => $car->id,
            'color' => $data['color']
        ]);
    }
}
