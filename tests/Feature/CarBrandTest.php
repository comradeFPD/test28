<?php

namespace Tests\Feature;

use App\Models\CarBrand;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CarBrandTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /**
     * @test Can I create new car brand
     *
     * @return void
     */
    public function canICreateCarBrand() :void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $data = CarBrand::factory()->make()->toArray();
        $response = $this->postJson(route('car-brand.store'), $data);
        $response->assertCreated();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseHas('car_brands', [
            'name' => $data['name']
        ]);
    }

    /**
     * @test Can I update existing car brand
     *
     * @return void
     */
    public function canIUpdateCarBrand() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $brand = CarBrand::factory()->create();
        $data = $brand->toArray();
        $data['name'] = 'new name';
        $response = $this->putJson(route('car-brand.update', $brand->id), $data);
        $response->assertOk();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseMissing('car_brands', [
            'name' => $brand->name
        ]);
        $this->assertDatabaseHas('car_brands', [
            'id' => $brand->id,
            'name' => $data['name']
        ]);
    }

    /**
     * @test Can I delete car brand
     *
     * @return void
     */
    public function canIDeleteCarBrand() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $brand = CarBrand::factory()->create();
        $response = $this->deleteJson(route('car-brand.destroy', $brand->id));
        $response->assertSuccessful();
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseMissing('car_brands', [
            'id' => $brand->id
        ]);
    }

    /**
     * @test Can I see all car brands
     *
     * @return void
     */
    public function canISeeAllCarBrands() : void
    {
        CarBrand::factory()->count(10)->create();
        $response = $this->getJson(route('car-brand.index'));
        $response->assertOk();
    }

    /**
     * @test Can I see single car brand
     *
     * @return void
     */
    public function canISeeSingleCarBrand() : void
    {
        $brand = CarBrand::factory()->create();
        $response = $this->getJson(route('car-brand.show', $brand->id));
        $response->assertOk();
    }

    /**
     * @test Can I create car brand without unique name
     *
     * @return void
     */
    public function canICreateBrandWithNotUniqueName() : void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $brand = CarBrand::factory()->create();
        $data = CarBrand::factory()->make([
            'name' => $brand->name
        ])->toArray();
        $response = $this->postJson(route('car-brand.store'), $data);
        $response->assertUnprocessable();
        $response->assertJsonValidationErrors(['name']);
        $this->assertEquals(1, CarBrand::query()->count());
    }
}
