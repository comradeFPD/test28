<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property-read int $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class CarBrand extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function cars() : HasMany
    {
        return $this->hasMany(Car::class, 'brand_id', 'id');
    }

    public function models() : HasMany
    {
        return $this->hasMany(CarModel::class, 'brand_id', 'id');
    }
}
