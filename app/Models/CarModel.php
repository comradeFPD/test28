<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property-read int $id
 * @property int $brand_id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $update_at
 */
class CarModel extends Model
{
    use HasFactory;

    protected $guarded = [
        'brand_id'
    ];

    /**
     * Brand relationship
     *
     * @return BelongsTo
     */
    public function brand() : BelongsTo
    {
        return $this->belongsTo(CarBrand::class, 'brand_id', 'id');
    }

    /**
     * Cars relationship
     *
     * @return HasMany
     */
    public function cars() : HasMany
    {
        return $this->hasMany(Car::class, 'model_id', 'id');
    }
}
