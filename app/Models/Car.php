<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property-read int $id
 * @property int|null $mileage
 * @property string|null $color
 * @property int $brand_id
 * @property int $model_id
 * @property int $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $production_year
 */
class Car extends Model
{
    use HasFactory;

    protected $guarded = [
        'brand_id',
        'model_id',
        'user_id'
    ];

    /**
     * User relationship
     *
     * @return BelongsTo
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Model relationship
     *
     * @return BelongsTo
     */
    public function model() : BelongsTo
    {
        return $this->belongsTo(CarModel::class, 'model_id', 'id');
    }

    /**
     * Brand relationship
     *
     * @return BelongsTo
     */
    public function brand() : BelongsTo
    {
        return $this->belongsTo(CarBrand::class, 'brand_id', 'id');
    }
}
