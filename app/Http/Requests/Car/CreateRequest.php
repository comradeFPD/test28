<?php

namespace App\Http\Requests\Car;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'brand_id'          => 'required|exists:car_brands,id',
            'model_id'          => 'required|exists:car_models,id',
            'color'             => 'nullable|max:255',
            'production_year'   => 'nullable|date',
            'mileage'           => 'nullable|numeric'
        ];
    }
}
