<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Brand\CreateRequest;
use App\Http\Resources\Brand\BrandResource;
use App\Http\Resources\Brand\BrandsCollection;
use App\Interfaces\BrandInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandController extends Controller
{
    private BrandInterface $service;

    public function __construct(BrandInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResource
     */
    public function index() : JsonResource
    {
        return BrandsCollection::make($this->service->index());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     *
     * @return JsonResource
     */
    public function store(CreateRequest $request) : JsonResource
    {
        return BrandResource::make($this->service->create($request->validated()));
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return JsonResource
     */
    public function show(string $id) : JsonResource
    {
        return BrandResource::make($this->service->view($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CreateRequest $request
     * @param string $id
     *
     * @return JsonResource
     */
    public function update(CreateRequest $request, string $id) : JsonResource
    {
        return BrandResource::make($this->service->update($id, $request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function destroy(string $id) : JsonResponse
    {
        $this->service->delete($id);
        return response()->json('', 204);
    }
}
