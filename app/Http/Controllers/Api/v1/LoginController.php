<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Login
     *
     * @param LoginRequest $request
     *
     * @return JsonResponse
     */
    public function __invoke(LoginRequest $request) : JsonResponse
    {
        if(Auth::attempt($request->validated())){
            $user = User::query()->where('email', $request->email)->first();
            return response()->json([
                'token' => $user->createToken('auth')
            ]);
        }
        return response()->json([
            'error' => 'Invalid data'
        ], 422);
    }
}
