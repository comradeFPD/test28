<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarModel\CreateRequest;
use App\Http\Resources\CarModel\CarModelResource;
use App\Http\Resources\CarModel\CarModelsCollection;
use App\Interfaces\CarModelInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ModelController extends Controller
{
    private CarModelInterface $service;

    public function __construct(CarModelInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResource
     */
    public function index() : JsonResource
    {
        return CarModelsCollection::make($this->service->index());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     *
     * @return JsonResource
     */
    public function store(CreateRequest $request) : JsonResource
    {
        return CarModelResource::make($this->service->create($request->validated()));
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return JsonResource
     */
    public function show(string $id) : JsonResource
    {
        return CarModelResource::make($this->service->view($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param string $id
     * @param CreateRequest $request
     *
     * @return JsonResource
     */
    public function update(CreateRequest $request, string $id) : JsonResource
    {
        return CarModelResource::make($this->service->update($id, $request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function destroy(string $id) : JsonResponse
    {
        $this->service->delete($id);
        return response()->json('', 204);
    }
}
