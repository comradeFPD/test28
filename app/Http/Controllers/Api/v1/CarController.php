<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Car\CreateRequest;
use App\Http\Resources\Car\CarResource;
use App\Http\Resources\Car\CarsCollection;
use App\Interfaces\CarInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarController extends Controller
{
    private CarInterface $service;

    public function __construct(CarInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResource
     */
    public function index() : JsonResource
    {
        return CarsCollection::make($this->service->index());
    }

    /**
     * Store a newly created resource in storage.\
     *
     * @param CreateRequest $request
     *
     * @return JsonResource
     */
    public function store(CreateRequest $request) : JsonResource
    {
        return CarResource::make($this->service->create($request->validated()));
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return JsonResource
     */
    public function show(string $id) : JsonResource
    {
        return CarResource::make($this->service->view($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateRequest $request
     * @param string $id
     *
     * @return JsonResource
     */
    public function update(CreateRequest $request, string $id) : JsonResource
    {
        return CarResource::make($this->service->update($id, $request->validated()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function destroy(string $id) : JsonResponse
    {
        $this->service->delete($id);
        return response()->json('', 204);
    }

    /**
     * View my cars
     *
     * @return JsonResource
     */
    public function viewMyCars() : JsonResource
    {
        return CarsCollection::make($this->service->viewAllMyCar());
    }
}
