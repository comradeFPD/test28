<?php

namespace App\Interfaces;

use App\Models\CarBrand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface BrandInterface
{
    /**
     * Create new brand
     *
     * @param array $data
     *
     * @return CarBrand|Model
     */
    public function create(array $data) : CarBrand|Model;

    /**
     * Update existing brand
     *
     * @param int $id
     * @param array $data
     *
     * @return CarBrand|Model
     */
    public function update(int $id, array $data) : CarBrand|Model;

    /**
     * Delete single brand
     *
     * @param int $id
     *
     * @return void
     */
    public function delete(int $id) : void;

    /**
     * Get single brand
     *
     * @param int $id
     *
     * @return CarBrand|Model
     */
    public function view(int $id) : CarBrand|Model;

    /**
     * Get all brands
     *
     * @return Collection
     */
    public function index() : Collection;
}
