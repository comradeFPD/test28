<?php

namespace App\Interfaces;

use App\Models\CarModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface CarModelInterface
{
    /**
     * Create new car model
     *
     * @param array $data
     *
     * @return CarModel|Model
     */
    public function create(array $data) : CarModel|Model;

    /**
     * Update existing car model
     *
     * @param int $id
     * @param array $data
     *
     * @return CarModel|Model
     */
    public function update(int $id, array $data) : CarModel|Model;

    /**
     * Delete car model
     *
     * @param int $id
     *
     * @return void
     */
    public function delete(int $id) : void;

    /**
     * View single car model
     *
     * @param int $id
     *
     * @return CarModel|Model
     */
    public function view(int $id) : CarModel|Model;

    /**
     * View all car models
     *
     * @return Collection
     */
    public function index() : Collection;
}
