<?php

namespace App\Interfaces;

use App\Models\Car;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface CarInterface
{
    /**
     * Create new car
     *
     * @param array $data
     *
     * @return Car|Model
     */
    public function create(array $data) : Car|Model;

    /**
     * Update existing car
     *
     * @param int $id
     * @param array $data
     *
     * @return Car|Model
     */
    public function update(int $id, array $data) : Car|Model;

    /**
     * Delete car
     *
     * @param int $id
     *
     * @return void
     */
    public function delete(int $id) : void;

    /**
     * View single car
     *
     * @param int $id
     *
     * @return Car|Model
     */
    public function view(int $id) : Car|Model;

    /**
     * View all cars
     *
     * @return Collection
     */
    public function index() : Collection;

    /**
     * View all my cars
     *
     * @return Collection
     */
    public function viewAllMyCar() : Collection;
}
