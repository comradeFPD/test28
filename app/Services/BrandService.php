<?php

namespace App\Services;

use App\Interfaces\BrandInterface;
use App\Models\CarBrand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BrandService implements BrandInterface
{

    /**
     * @inheritDoc
     */
    public function create(array $data): CarBrand|Model
    {
        return CarBrand::query()->create($data);
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data): CarBrand|Model
    {
        $brand = CarBrand::query()->findOrFail($id);
        $brand->update($data);
        return $brand;
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $brand = CarBrand::query()->findOrFail($id);
        $brand->delete();
    }

    /**
     * @inheritDoc
     */
    public function view(int $id): CarBrand|Model
    {
        return CarBrand::query()->findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function index(): Collection
    {
        return CarBrand::all();
    }
}
