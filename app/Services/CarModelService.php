<?php

namespace App\Services;

use App\Interfaces\CarModelInterface;
use App\Models\CarModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CarModelService implements CarModelInterface
{

    /**
     * @inheritDoc
     */
    public function create(array $data): CarModel|Model
    {
        $model = new CarModel();
        $model->fill($data);
        $model->brand_id = $data['brand_id'];
        $model->save();
        return $model;
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data): CarModel|Model
    {
        /**
         * @var CarModel $model
         */
        $model = CarModel::query()->findOrFail($id);
        $model->fill($data);
        $model->brand_id = $data['brand_id'];
        $model->save();
        return $model;
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $model = CarModel::query()->findOrFail($id);
        $model->delete();
    }

    /**
     * @inheritDoc
     */
    public function view(int $id): CarModel|Model
    {
        return CarModel::query()->findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function index(): Collection
    {
        return CarModel::all();
    }
}
