<?php

namespace App\Services;

use App\Interfaces\CarInterface;
use App\Models\Car;
use App\Models\User;
use Illuminate\Auth\AuthManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CarService implements CarInterface
{

    private AuthManager $manager;

    public function __construct(AuthManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Car|Model
    {
        $car = new Car();
        $car->fill($data);
        $car->brand_id = $data['brand_id'];
        $car->model_id = $data['model_id'];
        $car->user_id = $this->manager->guard('sanctum')->user()->id;
        $car->save();
        return $car;
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data): Car|Model
    {
        /**
         * @var User $user
         */
        $user = $this->manager->guard('sanctum')->user();
        $car = $user->cars()->findOrFail($id);
        $car->fill($data);
        $car->brand_id = $data['brand_id'];
        $car->model_id = $data['model_id'];
        $car->save();
        return $car;
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        /**
         * @var User $user
         */
        $user = $this->manager->guard('sanctum')->user();
        $car = $user->cars()->findOrFail($id);
        $car->delete();
    }

    /**
     * @inheritDoc
     */
    public function view(int $id): Car|Model
    {
        return Car::query()->findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function index(): Collection
    {
        return Car::all();
    }

    /**
     * @inheritDoc
     */
    public function viewAllMyCar(): Collection
    {
        /**
         * @var User $user
         */
        $user = $this->manager->guard('sanctum')->user();
        return $user->cars;
    }
}
