<?php

namespace App\Providers;

use App\Interfaces\BrandInterface;
use App\Interfaces\CarInterface;
use App\Interfaces\CarModelInterface;
use App\Services\BrandService;
use App\Services\CarModelService;
use App\Services\CarService;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(BrandInterface::class, function() {
            return new BrandService();
        });
        $this->app->bind(CarModelInterface::class, function() {
            return new CarModelService();
        });
        $this->app->bind(CarInterface::class, function() {
            return new CarService(app(AuthManager::class));
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
